$( document ).ready(function() {
  $('.card-back').hide()
  removeMobileTransparency();

  $(window).resize(function() {
    removeMobileTransparency();
  });

  $(function() {
    $('.card').hover(function() { 
        $(this).find('.card-front').stop(true).hide(0);
        $(this).find('.card-back').stop(true).fadeIn(400); 

    }, function() { 
        $('.card-back').stop(true).hide(0); 
        $(this).find('.card-front').stop(true).fadeIn(400); 
    });
  });
});

function removeMobileTransparency() {
  if($(window).width() < 768){
    $('.navbar-default').removeClass('navbar-transparent');
    $('#navbar').removeClass('navbar-transparent');
  }
}