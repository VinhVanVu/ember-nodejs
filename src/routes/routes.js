var express = require('express');
var router = express.Router();

// initial wildcard get to differentiate between root domain and subdomains.
router.get('*', function(req, res, next){
    if(req.headers.host == 'localhost:3000') {
        req.url = '/main' + req.url;
    } 
    if(req.headers.host == 'academy.localhost:3000') {
        req.url = '/academy' + req.url;
    }  
    next();
});

/* www.ember.gg */
router.get('/main', function(req, res, next) {
  res.render('main/index', { 
    title: 'Ember - League of Legends NA Challenger Series Team',
    id: 'HOME'});
});

router.get('/main/news', function(req, res, next) {
  res.render('main/news', { 
    title: 'Ember News - Latest News About LoL NACS Team Ember',
    id: 'NEWS' });
});

router.get('/main/teams', function(req, res, next) {
  res.render('main/teams', { 
    title: 'Ember\'s Team - Find Out Who\'s on Ember\'s Roster',
    id: 'TEAMS' });
});

router.get('/main/about', function(req, res, next) {
  res.render('main/about', { 
    title: 'About Ember - Find Out More About LoL NACS Team Ember',
    id: 'ABOUT' });
});

router.get('/main/academy', function(req, res, next) {
  res.redirect('academy.localhost:3000');
});

/* academy.ember.gg */
router.get('/academy', function(req, res, next) {
  res.render('academy/index', { 
    title: 'Ember Academy' });
});

router.get('/academy/faq', function(req, res, next) {
  res.render('academy/faq', { 
    title: 'Ember Academy - Frequently Asked Questions' });
});

module.exports = router;
